<?php

namespace App\Repository;

use App\Service\XmlService;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class CacheRepository
{
    /**
     * @param $date
     * @return array
     * @throws InvalidArgumentException
     */
    public function loadCachedDataByDate($date): array
    {
        $date = date('d.m.Y', strtotime($date));
        $cachePool = new FilesystemAdapter();
        $data = [
            'success' => false,
            'data' => []
        ];
        if ($cachePool->hasItem($date)) {
            $demoOne = $cachePool->getItem($date);
            $cachedData = $demoOne->get();
            $data['success'] = true;
            $data['data'] = $cachedData[$date];
        }

        return $data;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function saveToCacheData($data): void
    {
        $xmlService = new XmlService;
        $cachePool = new FilesystemAdapter();
        $currentDate = $xmlService->xml2array($data['Date'])[0];
        $result[$currentDate] = [];
        foreach ($data->Valute as $item) {
            $data = $xmlService->xml2array($item);

            $tmp[$data['CharCode']] = [
                'id' => $data['@attributes']['ID'],
                'NumCode' => $data['NumCode'],
                'CharCode' => $data['CharCode'],
                'Nominal' => $data['Nominal'],
                'Name' => $data['Name'],
                'Value' => $data['Value'],
                'CurrentDate' => $currentDate
            ];
            $result[$currentDate] = $tmp;
        }

        $pool = $cachePool->getItem($currentDate);
        if (!$pool->isHit()) {
            $pool->set($result);
            $cachePool->save($pool);
        }
    }
}
