<?php

namespace App\Controller;

use App\Service\CbrService;
use App\Repository\CacheRepository;
use Psr\Cache\InvalidArgumentException;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\{Request, Response};
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api", name="api_")
 */
class CbrController extends AbstractController
{
    /**
     * @Route("/exchange", name="exchange", methods={"POST"})
     * @throws InvalidArgumentException
     * @throws GuzzleException
     */
    public function getCoursesByForeignCurrency(
        Request         $request,
        CacheRepository $cacheRepository,
        CbrService      $cbrService
    ): Response {
        $filtered = $this->configureOptions($request);

        $currency = $filtered['baseCurrency'];
        $foreignCurrency = $filtered['foreignCurrency'];
        $count = $filtered['count'];
        $dateString = $filtered['date'];

        $data = $cacheRepository->loadCachedDataByDate($dateString);
        if ($data['success'] === false) {
            //without cache
            $dataFromAPI = $cbrService->getCurrency(strtotime($dateString));
            $data['data']['result'] = $cbrService->getCurrencyByCharCode(
                $dataFromAPI,
                $currency,
                $foreignCurrency,
                $count,
                true
            );
            $data['data']['withCache'] = false;
        } else {
            //with cache
            $result['result'] = $cbrService->getCurrencyByCharCode(
                $data['data'],
                $currency,
                $foreignCurrency,
                $count
            );
            $data['data'] = $result;
            $data['data']['withCache'] = true;
        }
        $data['success'] = true;

        return $this->json($data);
    }

    /**
     * @param $request
     * @return array
     */
    public function configureOptions($request): array
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'baseCurrency' => 'RUR',
            'count' => '1',
        ]);
        $resolver->setRequired('count');
        $resolver->setRequired('date');
        $resolver->setRequired('foreignCurrency');

        return $resolver->resolve($request->request->all());
    }
}
