<?php

namespace App\Service;

class XmlService
{
    /**
     * @param $xmlObject
     * @param array $out
     * @return array
     */
    public function xml2array($xmlObject, array $out = array()): array
    {
        foreach ((array)$xmlObject as $index => $node)
            $out[$index] = (is_object($node)) ? $this->xml2array($node) : $node;

        return $out;
    }
}
