<?php

namespace App\Service;

use GuzzleHttp\Client;
use App\Repository\CacheRepository;
use Psr\Cache\InvalidArgumentException;
use GuzzleHttp\Exception\GuzzleException;

class CbrService
{
    /**
     * @throws GuzzleException|InvalidArgumentException
     */
    public function getCurrency($date): \SimpleXMLElement
    {
        $url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=';
        $url .= date('d/m/Y', $date);
        $client = new Client();
        $res = $client->get($url);

        $body = $res->getBody();
        $cacheRepository = new CacheRepository();
        $cacheRepository->saveToCacheData(simplexml_load_string($body));

        return simplexml_load_string($body);
    }

    /**
     * @param $data
     * @param $currency
     * @param $foreignCurrency
     * @param $count
     * @param bool $isFromAPI
     * @return array
     */
    public function getCurrencyByCharCode(
        $data,
        $currency,
        $foreignCurrency,
        $count,
        bool $isFromAPI = false
    ): array {
        $xmlService = new XmlService;
        $preparedData = [];

        if ($currency == 'RUR')
            $preparedData['currency']['CharCode'] = 'RUR';
            $preparedData['currency']['Name'] = 'Российских рублей';
        if ($foreignCurrency == 'RUR')
            $preparedData['foreignCurrency']['CharCode'] = 'RUR';
            $preparedData['foreignCurrency']['Name'] = 'Российских рублей';
        if ($isFromAPI) {
            $currentDate = $xmlService->xml2array($data['Date'])[0];
            foreach ($data->Valute as $item) {
                $data = $xmlService->xml2array($item);
                if ($foreignCurrency == $data['CharCode']) {
                    $preparedData['foreignCurrency'] = $data;
                }
                if ($currency == $data['CharCode']) {
                    $preparedData['currency'] = $data;
                }
            }
            $preparedData['date'] = $currentDate;
        } else {
            foreach ($data as $item) {
                if ($foreignCurrency == $item['CharCode']) {
                    $preparedData['foreignCurrency'] = $item;
                }
                if ($currency == $item['CharCode']) {
                    $preparedData['currency'] = $item;
                }
                $preparedData['date'] = $item['CurrentDate'];
            }
        }

        $preparedData['count'] = $count;
        return $this->changeRate($preparedData);
    }

    /**
     * @param $preparedData
     * @return array
     */
    private function changeRate($preparedData): array
    {

        $foreignCurrency = $preparedData['foreignCurrency']['Value'] ?? 1;
        $foreignCurrency = str_replace(",", ".", $foreignCurrency);

        $currency = $preparedData['currency']['Value'] ?? 1;
        $currency = str_replace(",", ".", $currency);

        $currencyNominal = $preparedData['currency']['Nominal'] ?? 1;
        $foreignCurrencyNominal = $preparedData['foreignCurrency']['Nominal'] ?? 1;

        $oneCurrency = $currency / $currencyNominal;
        $oneForeignCurrency = $foreignCurrency / $foreignCurrencyNominal;

        $count = (integer)$preparedData['count'] ?? 1;
        $text = $count;
        $count = $oneCurrency / $oneForeignCurrency * $count;
        $text .= ' ' . $preparedData['currency']['Name'] . ' = ' . $count;
        $text .= ' ' . $preparedData['foreignCurrency']['Name'];
        return [
            'total' => $text,
            'date' => $preparedData['date'] ?? ''
        ];
    }
}
