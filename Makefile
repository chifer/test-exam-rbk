install:
	cp .env.test .env && composer install && docker-compose build && docker-compose up -d

run:
	docker-compose up -d

runDev:
	docker-compose up

stop:
	docker-compose down
