FROM php

RUN apt-get update && apt-get install -y \
    wget \
    sudo \
    libpq-dev \
    git \
    zip \
    unzip

RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | sudo -E bash && \
    apt-get install symfony-cli -y

WORKDIR /app/
COPY ./ /app/

RUN composer install

CMD [ "symfony", "server:start"]
