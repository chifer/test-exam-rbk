# Little exercise for RBC

Author: Ismailov Ruslan

Email: chifek@gmail.com

## Installation

1. Use the [docker engine](https://docs.docker.com/engine/install/ubuntu/) to install the project.

2. Import POSTMAN collection from file 'RBC.postman_collection.json' (default port 8000)

```bash
make install
```

## Usage

Run without logs
```bash
make run
```

Run with logs
```bash
make runDev
```

Stop project
```bash
make stop
```
